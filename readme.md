# Welcome To Sequis Mobile Engineer Tests
Welcome Candidate!!, This is the Homework test for candidates who want to join Sequis life mobile apps engineers.  
Today you will be given an assignment to create a simple Image browser application, with add comment features

**Here are the ground rules:**

1. Do this assignment using native code / programming language
    * iOS -> Swift / SwiftUI
    * Android -> Kotlin
2. The **Assignment** will be split into **Tasks**, and it will be explained using a **Story**, Candidate needs to fulfill the **Acceptance Criteria**  
3. The provided screen shots are captured using iOS Simulator, if you are an Android Developer. the layout is allowed to be slightly different  
4. Any frameworks are welcomed, you are free to use any frameworks or library that available from internet  
5. If possible, please use all your knowledge when creating this assignment, any of it will bring extra points  
   (e.g:  **Architecture**, **Unit Test**, **Clean Code**, **Patterns**, etc )  
6. There is no fixed specification about padding / margin constant, please use your creativity to create a layout similar to the provided screen shot   
   ~~**(in screen shot using 4(n),- 4, 8, 12, 16, and so on)**~~
7. Create your own repository for submitting the assignment and commit your project periodically 
8. Time limit for this assignment: maximum 2 days 
9. at last but not least, **Good Luck!**   

## Screens
### 1. Main Screen
![Screen 1](https://i.imgur.com/qfZQ7pG.png?1)

### 1.1 Acceptance Criteria
```gherkin
Scenario: Launch Screen 
Given Installed App
When User open the App
Then Show Main screen that display the list of the images 
And Images will be displayed in a Card layout (with rounded view and shadow)
And The card also contains the Author's name
```

### 1.2 Acceptance Criteria
```gherkin
Scenario: Infinite Scroll View
Given User is on Main Screen
When User scroll down to the end of the list
Then Get more images from API
```

### 1.3 Acceptance Criteria
```gherkin
Scenario: Select Card
Given User is on Main Screen
When User select any image card
Then Open detail screen
```

**Notes:**
Use this Api to get the Images, please read the api [documentation](https://picsum.photos/)  
Get Image List: `https://picsum.photos/v2/list`  
Sample Api Response   
```json
[
  {
    "id": "0",
    "author": "Alejandro Escamilla",
    "width": 5616,
    "height": 3744,
    "url": "https://unsplash.com/photos/yC-Yzbqy7PY",
    "download_url": "https://picsum.photos/id/0/5616/3744"
  },
  ...
]
```

## 2. Detail screen
![Imgur](https://i.imgur.com/AtJ9bSD.png?1)
### 2.1 Acceptance Criteria
```gherkin
Scenario: Initial Detail Screen 
Given An image card from Main Screen is selected
When Detail Screen is shown
Then Show the image in bigger size
And show the comments if available (sorted from the last added comment)
And show add comment (+) button
```

#### 2.2 Acceptance Criteria
```gherkin
Scenario: Add Comment
Given User is on Detail Screen
When User tap on add comment (+) Button
Then Add new comment, and show it under the image
```

### 2.3 Acceptance Criteria
```gherkin
Scenario: Creating a new Comment
Given Add comment (+) Button is Tapped
When Creating new comment
Then the comment card should be displayed as shown in the screen shot
And the comment's content (name and comment) will be randomly generated using provided json file
And show the date with relative date
```

### 2.4 Acceptance Criteria
```gherkin
Scenario: Delete Comment
Given User is on Detail Screen
And There is at least 1 comment
When User tap delete on a comment 
Then Remove the comment
```

### 2.5 Acceptance Criteria
```gherkin
Scenario: Re-open the apps
Given User re-open the application
And There are images with added comments (from previous session) 
When User select one of the those image card
Then Show the existing comments
```
**Notes:**

Use these json files, inside your project to generate names and content: - [directory](resources/)

* [firstNames.json](resources/firstNames.json)
* [lastNames.json](resources/lastNames.json)
* [verbs.json](resources/verbs.json)
* [nouns.json](resources/nouns.json)

for point 2.4, please feel free to use any default animation / behavior from library

### TLDR; App Features:
- Get Image from the API, and show it using Infinite Scroll View
- Image should load asynchronously  
![](https://media.giphy.com/media/l705K0BeYZ9Ztw3yFe/giphy.gif)
- Click any list, will show Image Detail with comment
- Add generated comment and store it in database 
- Show created date with relative date  
![](https://media.giphy.com/media/rIwrnyMnoH4kSVInYL/giphy.gif)
- Add delete feature using Swipe (or any similar behavior)  
![](https://media.giphy.com/media/8VBotkzqEp4RuWSGgi/giphy.gif)

## That's all Folks
should there be any question please email us in [mobile_developer@inlab.sequislife.com](mailto:mobile_developer@inlab.sequislife.com)
